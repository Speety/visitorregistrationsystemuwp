﻿using System;
using Windows.UI.Xaml.Data;

namespace VRSystem.Helpers.Converters
{
    public class CapitalizeFirstLetterConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            // this will be called after getting the value from your backing property
            // and before displaying it in the textbox, so we just pass it as-is
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            // this will be called after the textbox loses focus (in this case) and
            // before its value is passed to the property setter, so we make our
            // change here
            if (value is string)
            {
                var stringValue = value.ToString();

                if(!string.IsNullOrEmpty(stringValue))
                {
                    var castValue = (string)value;
                    return char.ToUpper(castValue[0]) + castValue.Substring(1);
                }

                return stringValue;
            }
            else
            {
                return value;
            }
        }
    }
}
