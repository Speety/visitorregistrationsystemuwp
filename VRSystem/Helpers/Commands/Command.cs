﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Windows.UI.Xaml;

namespace VRSystem.Helpers.Commands
{
    public class Command<T> : DependencyObject, ICommand
    {

        public event EventHandler CanExecuteChanged;
        public event EventHandler<T> Executed;


        public bool CanExecute(object parameter)
        {
            return this.IsEnabled;
        }

        public void Execute(object parameter = null)
        {
            if (Executed != null)
                Executed.Invoke(this, (T)parameter);
        }

        public bool IsEnabled
        {
            get { return (bool)GetValue(IsEnabledProperty); }
            set { SetValue(IsEnabledProperty, value); }
        }

        public static readonly DependencyProperty IsEnabledProperty = DependencyProperty.Register("IsEnabled", typeof(bool), typeof(Command<T>), new PropertyMetadata(true, (d, e) =>
        {
            var command = d as Command<T>;
            if (command.CanExecuteChanged != null)
                command.CanExecuteChanged.Invoke(command, new EventArgs());
        }));

    }

    public class Command : DependencyObject, ICommand
    {

        public event EventHandler CanExecuteChanged;
        public event DoWorkEventHandler Executed;


        public bool CanExecute(object parameter)
        {
            return this.IsEnabled;
        }

        public void Execute(object parameter = null)
        {
            if (Executed != null)
                Executed.Invoke(this, new DoWorkEventArgs(parameter));
        }

        public bool IsEnabled
        {
            get { return (bool)GetValue(IsEnabledProperty); }
            set { SetValue(IsEnabledProperty, value); }
        }

        public static readonly DependencyProperty IsEnabledProperty = DependencyProperty.Register("IsEnabled", typeof(bool), typeof(Command), new PropertyMetadata(true, (d, e) =>
        {
            var command = d as Command;
            if (command.CanExecuteChanged != null)
                command.CanExecuteChanged.Invoke(command, new EventArgs());
        }));

    }
}
