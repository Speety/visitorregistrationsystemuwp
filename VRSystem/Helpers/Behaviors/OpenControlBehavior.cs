﻿
namespace VRSystem.Helpers.Behaviors
{
    using Microsoft.Xaml.Interactivity;
    using System;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Media.Animation;

    public class OpenControlBehavior : Behavior<FrameworkElement>
    {
        private double newValue;
        public static readonly DependencyProperty PropertyNameProperty = DependencyProperty.Register(
            "PropertyName", typeof(string), typeof(OpenControlBehavior), new PropertyMetadata("Width"));

        public static readonly DependencyProperty IsOpenProperty = DependencyProperty.Register(
           "IsOpen", typeof(bool), typeof(OpenControlBehavior), new PropertyMetadata(false, IsOpenChanged));

        public bool IsOpen
        {
            get { return (bool)GetValue(IsOpenProperty); }
            set { SetValue(IsOpenProperty, value); }
        }

        public string PropertyName
        {
            get { return (string)GetValue(PropertyNameProperty); }
            set { SetValue(PropertyNameProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            var propertyName = (string)AssociatedObject.GetValue(PropertyNameProperty);
            if (this.AssociatedObject != null && !string.IsNullOrEmpty(propertyName))
            {
                this.newValue = propertyName == "Width" ? Window.Current.Bounds.Width : Window.Current.Bounds.Height;
                var value = this.IsOpen ? newValue : 0;
                AssociatedObject.SetValue(PropertyNameProperty, value);
                this.AssociatedObject.Visibility = this.IsOpen ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
        }

        private static void IsOpenChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as OpenControlBehavior;
            var newValue = (bool)e.NewValue;
            var oldValue = (bool)e.OldValue;
            if (control != null && control.AssociatedObject != null && newValue != oldValue)
            {
                ((OpenControlBehavior)d).OpenAnimation(newValue);
            }
        }

        public void OpenAnimation(bool isOpen)
        {
            if (isOpen)
            {
                this.AssociatedObject.Visibility = Visibility.Visible;
            }

            double to = isOpen ? this.newValue : 0;
            double from = isOpen ? 0 : this.newValue;

            var storyboard = new Storyboard();
            var easingFunction = new ExponentialEase();
            easingFunction.EasingMode = EasingMode.EaseOut;

            var animation = new DoubleAnimation()
            {
                To = to,
                From = from,
                EnableDependentAnimation = true,
                Duration = new Duration(new TimeSpan(days: 0, hours: 0, minutes: 0, seconds: 0, milliseconds: 200)),
                EasingFunction = easingFunction
            };

            Storyboard.SetTarget(animation, AssociatedObject);
            Storyboard.SetTargetProperty(animation, this.PropertyName);
            storyboard.Children.Add(animation);
            storyboard.Begin();
            if (!isOpen)
            {
                storyboard.Completed += (sender, param) =>
                {
                    this.AssociatedObject.Visibility = isOpen ? Visibility.Visible : Visibility.Collapsed;
                };
            }
        }
    }
}
