﻿using System;
using Windows.UI.Xaml;

namespace VRSystem.Resources
{
    public static class ResourceHelper
    {
        const string viewTemplateAddress = "ms-appx:///Resources/Templates/Views/";

        public static DataTemplate GetViewModelTemplate(string resourceDictionary, object key)
        {
            var dictionary = new ResourceDictionary();
            dictionary.Source = new Uri(viewTemplateAddress + resourceDictionary, UriKind.Absolute);
            return dictionary[key] as DataTemplate;
        }
    }
}
