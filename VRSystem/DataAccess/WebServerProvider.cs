﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using VRSystem.Models;
using VRSystem.ViewModels;

namespace VRSystem.DataAccess
{
    public static class WebServerProvider
    {
        static string apiUrl = "http://localhost:65189/api/values/";

        private static string apiKey = "AdYhf918Uil_diIhG8891149056Jk-oOpjhHGywAwFVdAX-dd3JoTGfda0";

        private static string authanticationValueFirst = "KoIhhda29JknNbtgAgwVXKHyftTP";
        private static string authanticationValueSecond = "LoOuhA09Y63529Hh8G5";

        public static async Task<List<EmployeeJson>> InitEmployees()
        {
            List<EmployeeJson> result = null;

            string requestLink = apiUrl + "InitEmployees";
            HttpClient httpClient = new HttpClient();
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, requestLink);
            requestMessage.Headers.Add("API_KEY", apiKey);
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(
                                                                                           System.Text.ASCIIEncoding.ASCII.GetBytes(
                                                                                           string.Format("{0}:{1}", authanticationValueFirst, authanticationValueSecond))));

            try
            {
                Task<HttpResponseMessage> httpRequest = httpClient.SendAsync(requestMessage, HttpCompletionOption.ResponseContentRead, CancellationToken.None);
                var resultJson = await httpRequest.Result.Content.ReadAsStringAsync();
                result = JsonConvert.DeserializeObject<List<EmployeeJson>>(resultJson);
            }
            catch (Exception e)
            {
                var em = e.Message.ToString();
            }

            return result;
        }

        public static async Task<List<Visitor>> InitVisiters()
        {
            List<Visitor> result = null;

            string requestLink = apiUrl + "InitVisiters";
            HttpClient httpClient = new HttpClient();
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, requestLink);
            requestMessage.Headers.Add("API_KEY", apiKey);
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(
                                                                                           System.Text.ASCIIEncoding.ASCII.GetBytes(
                                                                                           string.Format("{0}:{1}", authanticationValueFirst, authanticationValueSecond))));

            try
            {
                Task<HttpResponseMessage> httpRequest = httpClient.SendAsync(requestMessage, HttpCompletionOption.ResponseContentRead, CancellationToken.None);
                var resultJson = await httpRequest.Result.Content.ReadAsStringAsync();

                if (!string.IsNullOrEmpty(resultJson))
                {
                    result = JsonConvert.DeserializeObject<List<Visitor>>(resultJson);
                }
            }
            catch (Exception e)
            {
                var em = e.Message.ToString();
            }

            return result;
        }
        
        public static async Task<ChangesMadeOnServer> CheckIfChangesMadeOnServer()
        {
            ChangesMadeOnServer result = null;

            string requestLink = apiUrl + "CheckIfChangesMadeOnServer";
            HttpClient httpClient = new HttpClient();
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, requestLink);
            requestMessage.Headers.Add("API_KEY", apiKey);
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(
                                                                                           System.Text.ASCIIEncoding.ASCII.GetBytes(
                                                                                           string.Format("{0}:{1}", authanticationValueFirst, authanticationValueSecond))));

            try
            {
                Task<HttpResponseMessage> httpRequest = httpClient.SendAsync(requestMessage, HttpCompletionOption.ResponseContentRead, CancellationToken.None);
                var stringResult = await httpRequest.Result.Content.ReadAsStringAsync();

                if(!string.IsNullOrEmpty(stringResult))
                {
                    result = JsonConvert.DeserializeObject<ChangesMadeOnServer>(stringResult);
                }
            }
            catch (Exception e)
            {
                var em = e.Message.ToString();
            }

            return result;
        }

        public static async Task<Visitor> RegisterVisit(Employee employeeToGo, NewVisitor visitorToGo)
        {
            Visitor result = null;

            HttpClient httpClient = new HttpClient();
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, apiUrl + "RegisterVisit");

            JsonConvertEntity entity = new JsonConvertEntity() { Employee = employeeToGo, Visitor = visitorToGo };
            var json = JsonConvert.SerializeObject(entity);
            StringContent stringContent = new StringContent(json);
            requestMessage.Content = stringContent;
            requestMessage.Headers.Add("API_KEY", apiKey);
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(
                                                                                           System.Text.ASCIIEncoding.ASCII.GetBytes(
                                                                                           string.Format("{0}:{1}", authanticationValueFirst, authanticationValueSecond))));

            try
            {
                Task<HttpResponseMessage> httpRequest = httpClient.SendAsync(requestMessage, HttpCompletionOption.ResponseContentRead, CancellationToken.None);
                var resultJson = await httpRequest.Result.Content.ReadAsStringAsync();

                if (!string.IsNullOrEmpty(resultJson))
                {
                    result = JsonConvert.DeserializeObject<Visitor>(resultJson);
                }

            }
            catch (Exception e)
            {
                var em = e.Message.ToString();
            }

            return result;
        }

        public static async Task<bool> CheckOutVisitor(Visitor visitorToCheckOut)
        {
            bool result = false;

            HttpClient httpClient = new HttpClient();
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, apiUrl + "CheckOutVisitor");

            var json = JsonConvert.SerializeObject(visitorToCheckOut);
            StringContent stringContent = new StringContent(json);
            requestMessage.Content = stringContent;
            requestMessage.Headers.Add("API_KEY", apiKey);
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(
                                                                                           System.Text.ASCIIEncoding.ASCII.GetBytes(
                                                                                           string.Format("{0}:{1}", authanticationValueFirst, authanticationValueSecond))));

            try
            {
                Task<HttpResponseMessage> httpRequest = httpClient.SendAsync(requestMessage, HttpCompletionOption.ResponseContentRead, CancellationToken.None);
                var resultString = await httpRequest.Result.Content.ReadAsStringAsync();

                if (!string.IsNullOrEmpty(resultString))
                {
                   result = Convert.ToBoolean(resultString);
                }

            }
            catch (Exception e)
            {
                var em = e.Message.ToString();
            }

            return result;
        }
    }
}
