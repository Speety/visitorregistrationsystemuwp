﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.Text;
using Microsoft.Toolkit.Uwp.Helpers;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using VRSystem.DataAccess;
using VRSystem.Helpers.Commands;
using VRSystem.Models;
using VRSystem.Models.Enums;
using VRSystem.Resources;
using VRSystem.ViewModels.Base;
using Windows.ApplicationModel;
using Windows.Foundation;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.System;
using Windows.UI;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using ZXing;
using ZXing.Mobile;
using ZXing.QrCode;

namespace VRSystem.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private const string viewTemplateKey = "MainViewTemplate";
        private bool isStartPressed;
        private bool isCheckOutPressed;
        private bool isCheckInPressed;
        private bool isNewNameVisitorPressed;
        private bool isNewVisitorCompanyNamePressed;
        private bool isNewVisitorPhonePressed;
        private bool isCheckOutSelectedVisitorPressed;
        private bool isErrorMessageShown;
        private string errorMessage;
        
        DispatcherTimer checkChangesTimer;
        private Visitor visitorToCheckOut;

        private Grid stickerToPringGrid;
        private BitmapImage imageToProintUri;
        private PrintHelper printHelper;


        public MainViewModel() : base()
        {
            this.templateDictionary = "MainView.xaml";

            this.OpenCloseMainCommand.Executed += OpenCloseMainCommand_Executed;
            this.OpenCloseCheckInCommand.Executed += OpenCloseCheckInCommand_Executed;
            this.OpenCloseCheckOutCommand.Executed += OpenCloseCheckOutCommand_Executed;

            this.SelectPersonCommand.Executed += SelectPersonCommand_Executed;
            this.SelectVisitorToCheckOutCommand.Executed += SelectVisitorToCheckOutCommand_Executed;

            this.OpenCloseNewNameVisitorCommand.Executed += OpenCloseNewNameVisitorCommand_Executed;
            this.OpenCloseNewVisitorCompanyNameCommand.Executed += OpenCloseNewVisitorCompanyNameCommand_Executed;
            this.OpenCloseNewVisitorPhoneCommand.Executed += OpenCloseNewVisitorPhoneCommand_Executed;
            this.PrintStickerCommand.Executed += PrintStickerCommand_Executed;

            this.CloseVisitorCheckOutCommand.Executed += CloseVisitorCheckOutCommand_Executed;
            this.CheckOutVisitorCommand.Executed += CheckOutVisitorCommand_Executed;

            this.ContactCommand.Executed += ContactCommand_Executed;
            
            var task = Refresh();

            this.PopulateEmployees();
            this.PopulateVisitors();
            this.InitCheckChangesTimer();
        }

        #region Commands

        public Command<bool> OpenCloseMainCommand { get; set; } = new Command<bool>();
        public Command<bool> OpenCloseCheckInCommand { get; set; } = new Command<bool>();
        public Command<bool> OpenCloseCheckOutCommand { get; set; } = new Command<bool>();

        public Command<Employee> SelectPersonCommand { get; set; } = new Command<Employee>();
        public Command<Visitor> SelectVisitorToCheckOutCommand { get; set; } = new Command<Visitor>();

        public Command<bool> OpenCloseNewNameVisitorCommand { get; set; } = new Command<bool>();
        public Command<bool> OpenCloseNewVisitorCompanyNameCommand { get; set; } = new Command<bool>();
        public Command<bool> OpenCloseNewVisitorPhoneCommand { get; set; } = new Command<bool>();
        public Command<Grid> PrintStickerCommand { get; set; } = new Command<Grid>();

        public Command CloseVisitorCheckOutCommand { get; set; } = new Command();
        public Command<string> CheckOutVisitorCommand { get; set; } = new Command<string>();

        public Command ContactCommand { get; set; } = new Command();

        #endregion

        #region Properties

        public string AppName { get; } = Package.Current.DisplayName;

        public bool IsStartPressed
        {
            get
            {
                return this.isStartPressed;
            }
            set
            {
                if (this.isStartPressed != value)
                {
                    this.isStartPressed = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        public bool IsCheckInPressed
        {
            get
            {
                return this.isCheckInPressed;
            }
            set
            {
                if (this.isCheckInPressed != value)
                {
                    this.isCheckInPressed = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        public bool IsCheckOutPressed
        {
            get
            {
                return this.isCheckOutPressed;
            }
            set
            {
                if (this.isCheckOutPressed != value)
                {
                    this.isCheckOutPressed = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        public bool IsNewNameVisitorPressed
        {
            get
            {
                return this.isNewNameVisitorPressed;
            }
            set
            {
                if (this.isNewNameVisitorPressed != value)
                {
                    this.isNewNameVisitorPressed = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        public bool IsNewVisitorCompanyNamePressed
        {
            get
            {
                return this.isNewVisitorCompanyNamePressed;
            }
            set
            {
                if (this.isNewVisitorCompanyNamePressed != value)
                {
                    this.isNewVisitorCompanyNamePressed = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        public bool IsNewVisitorPhonePressed
        {
            get
            {
                return this.isNewVisitorPhonePressed;
            }
            set
            {
                if (this.isNewVisitorPhonePressed != value)
                {
                    this.isNewVisitorPhonePressed = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        public bool IsCheckOutSelectedVisitorPressed
        {
            get
            {
                return this.isCheckOutSelectedVisitorPressed;
            }
            set
            {
                if (this.isCheckOutSelectedVisitorPressed != value)
                {
                    this.isCheckOutSelectedVisitorPressed = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        public bool IsErrorMessageShown
        {
            get
            {
                return this.isErrorMessageShown;
            }
            set
            {
                if (this.isErrorMessageShown != value)
                {
                    this.isErrorMessageShown = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                if (this.errorMessage != value)
                {
                    this.errorMessage = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        public BitmapImage ImageToProintUri
        {
            get
            {
                return this.imageToProintUri;
            }
            set
            {
                if (this.imageToProintUri != value)
                {
                    this.imageToProintUri = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        public ObservableCollection<Employee> PeopleToMeet { get; set; } = new ObservableCollection<Employee>();
        public ObservableCollection<Visitor> Visitors { get; set; } = new ObservableCollection<Visitor>();

        public Employee SelectedPerson { get; set; }

        public NewVisitor NewVisitor { get; set; }

        #endregion

        //MainScree
        private void OpenCloseMainCommand_Executed(object sender, bool newvalue)
        {
            this.IsStartPressed = newvalue;
        }

        //CheckIn
        private void OpenCloseCheckInCommand_Executed(object sender, bool newValue)
        {
            this.IsCheckInPressed = newValue;
        }

        private void SelectPersonCommand_Executed(object sender, Employee employee)
        {
            this.IsNewNameVisitorPressed = true;
            this.SelectedPerson = employee;

            this.NewVisitor = new NewVisitor();
            this.OnRefresh();
        }

        private void OpenCloseNewVisitorCompanyNameCommand_Executed(object sender, bool newValue)
        {
            this.ErrorMessage = string.Empty;
            this.IsErrorMessageShown = false;

            if (newValue)
            {
                if (this.IsValid(this.NewVisitor.FirstName, InputInformationType.NameOrLastName) && this.IsValid(this.NewVisitor.LastName, InputInformationType.NameOrLastName))
                {
                    this.IsNewVisitorCompanyNamePressed = newValue;
                }
            }
            else
            {
                this.IsNewVisitorCompanyNamePressed = newValue;
            }
        }

        private void OpenCloseNewNameVisitorCommand_Executed(object sender, bool newValue)
        {
            this.IsNewNameVisitorPressed = newValue;
        }

        private void OpenCloseNewVisitorPhoneCommand_Executed(object sender, bool newValue)
        {
            this.ErrorMessage = string.Empty;
            this.IsErrorMessageShown = false;

            if (newValue)
            {
                if (this.IsValid(this.NewVisitor.CompanyName, InputInformationType.CompanyName))
                {
                    this.IsNewVisitorPhonePressed = newValue;
                }
            }
            else
            {
                this.IsNewVisitorPhonePressed = newValue;
            }
        }

        //CheckOut
        private void OpenCloseCheckOutCommand_Executed(object sender, bool newValue)
        {
            this.IsCheckOutPressed = newValue;
        }

        //print sticker
        private async void PrintStickerCommand_Executed(object sender, Grid stickerToPringGrid)
        {
            this.ErrorMessage = string.Empty;
            this.IsErrorMessageShown = false;
            this.stickerToPringGrid = stickerToPringGrid;

            if (this.IsValid(this.NewVisitor.Phone, InputInformationType.PhoneNumber))
            {
                this.NewVisitor.CheckInTime = DateTime.Now;
                Visitor visitor = await WebServerProvider.RegisterVisit(this.SelectedPerson, this.NewVisitor);
                this.Visitors.Add(visitor);

                //generate sticker
                await this.GenerateSticker(visitor);

                this.IsNewVisitorPhonePressed = false;
                this.IsNewVisitorCompanyNamePressed = false;
                this.IsNewNameVisitorPressed = false;
                this.IsCheckInPressed = false;
            }
        }

        private void SelectVisitorToCheckOutCommand_Executed(object sender, Visitor visitor)
        {
            this.visitorToCheckOut = visitor;
            this.IsCheckOutSelectedVisitorPressed = true;
        }

        private void CloseVisitorCheckOutCommand_Executed(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            this.ErrorMessage = string.Empty;
            this.IsErrorMessageShown = false;
            this.visitorToCheckOut = null;
            this.IsCheckOutSelectedVisitorPressed = false;
        }

        private async void CheckOutVisitorCommand_Executed(object sender, string badgeNumber)
        {
            if (badgeNumber != null)
            {
                if (badgeNumber == this.visitorToCheckOut.BadgeNumber)
                {
                    var result = await WebServerProvider.CheckOutVisitor(this.visitorToCheckOut);

                    if (result)
                    {
                        this.Visitors.Remove(visitorToCheckOut);

                        this.visitorToCheckOut = null;
                        this.IsCheckOutSelectedVisitorPressed = false;
                        this.IsCheckOutPressed = false;
                    }
                }
                else
                {
                    //badge isn't correct
                    this.ErrorMessage = "Badge number isn't correct";
                    this.IsErrorMessageShown = true;
                }
            }
            else
            {
                //badge not provided
                this.ErrorMessage = "Please provide us your badge nmber";
                this.IsErrorMessageShown = true;
            }
        }

        private async void ContactCommand_Executed(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            if (this.IsConnected)
            {
                var text = "Contact message from Visitor Registration Client";
                var headerPattern = "{0}-{1}";
                var supportMail = "support.contact@gmail.com";
                var UriDecodedNewLine = "%0d%0a";

                var appVersion = Package.Current.Id.Version;

                var subject = string.Format(headerPattern, Package.Current.DisplayName, string.Format("{0}.{1}.{2}.{3}", appVersion.Major, appVersion.Minor, appVersion.Build, appVersion.Revision));
                var body = text + UriDecodedNewLine;

                body += UriDecodedNewLine + UriDecodedNewLine + UriDecodedNewLine + UriDecodedNewLine;

                subject = DeleteSystemUriSymbols(subject, true);
                body = DeleteSystemUriSymbols(body, false);

                var uriForLetter = new Uri("mailto:" + supportMail + "?Subject=" + subject + "&Body=" + body);

                await Launcher.LaunchUriAsync(uriForLetter);
            }
        }
         
        //Helper methods
        public void InitCheckChangesTimer()
        {
            checkChangesTimer = new DispatcherTimer() { Interval = new TimeSpan(0, 0, 30) };
            checkChangesTimer.Tick += Timer_Tick;
            checkChangesTimer.Start();
        }

        private async void Timer_Tick(object sender, object e)
        {
            var ChangesMadeOnServer = await WebServerProvider.CheckIfChangesMadeOnServer();

            if (ChangesMadeOnServer != null)
            {
                if(ChangesMadeOnServer.IsChangesMadeToEmployees)
                {
                    this.PopulateEmployees();
                }

                if(ChangesMadeOnServer.IsChangesMadeToVisitors)
                {
                    this.PopulateVisitors();
                }
            }
        }

        public async Task PopulateEmployees()
        {
            this.PeopleToMeet.Clear();
            var employeeList = await WebServerProvider.InitEmployees();

            foreach (var employee in employeeList)
            {
                this.PeopleToMeet.Add(new Employee()
                {
                    Name = employee.Name,
                    Position = employee.Position,
                    Id = employee.Id,
                    Photo = this.ImageFromBytes(employee.Photo)
                });
            }
        }

        public async Task PopulateVisitors()
        {
            this.Visitors.Clear();
            var visitersList = await WebServerProvider.InitVisiters();

            foreach (var visiter in visitersList)
            {
                this.Visitors.Add(visiter);
            }
        }

        protected async override Task<Exception> OnRefresh(object parameter = null)
        {
            Exception exception = null;
            try
            {
                SynchronizationContextProvider.UIThreadSyncContext.Post(_ =>
                {
                    this.ModelTemplate = ResourceHelper.GetViewModelTemplate(this.templateDictionary, viewTemplateKey);

                }, null);
            }
            catch (Exception ex)
            {
                exception = ex;
            }
            return exception;
        }

        private BitmapImage ImageFromBytes(byte[] bytes)
        {
            BitmapImage image;

            if (bytes != null)
            {
                image = new BitmapImage();

                using (InMemoryRandomAccessStream ms = new InMemoryRandomAccessStream())
                {
                    using (DataWriter writer = new DataWriter(ms.GetOutputStreamAt(0)))
                    {
                        writer.WriteBytes((byte[])bytes);
                        writer.StoreAsync().GetResults();
                    }
                    image.SetSource(ms);
                }
            }
            else
            {
                image = new BitmapImage(new Uri("ms-appx:///Assets/Icons/defaultUser.png"));
                image.DecodePixelHeight = 70;
                image.DecodePixelWidth = 70;
            }

            return image;
        }

        private bool IsValid(string stringToValidate, InputInformationType infoType)
        {
            bool result = false;

            if (!string.IsNullOrEmpty(stringToValidate))
            {
                if (infoType == InputInformationType.NameOrLastName)
                {
                    if (Regex.IsMatch(stringToValidate, @"^[a-zA-Z]+$"))
                    {
                        result = true;
                    }
                    else
                    {
                        this.ErrorMessage = "First Name and Last Name should contain only letters";
                        this.IsErrorMessageShown = true;
                    }
                }
                else if (infoType == InputInformationType.CompanyName)
                {
                    if (Regex.IsMatch(stringToValidate, @"^[a-zA-Z0-9]+$"))
                    {
                        result = true;
                    }
                    else
                    {
                        this.ErrorMessage = "Company Name should contain only letters or numbers";
                        this.IsErrorMessageShown = true;
                    }
                }
                else
                {
                    if (Regex.IsMatch(stringToValidate, @"\(?\d{3}\)?-? *\d{3}-? *-?\d{4}"))
                    {
                        result = true;
                    }
                    else
                    {
                        this.ErrorMessage = "Phone Number format isn't correct. Numbers only";
                        this.IsErrorMessageShown = true;
                    }
                }
            }
            else
            {
                this.ErrorMessage = "Please, provide requred information";
                this.IsErrorMessageShown = true;
            }

            return result;
        }

        private static string DeleteSystemUriSymbols(string data, bool isSubject)
        {
            data = data.Replace("#", "");
            data = data.Replace("&", "");
            if (isSubject)
            {
                data = data.Replace("%", "");
            }

            return data;
        }

        #region GenerateSticker

        private async Task GenerateSticker(Visitor visitor)
        {
            //generate qr
            this.ImageToProintUri = null;
            var qrWritableBitmap = this.GenerateQRCode(visitor);

            //Generate sticker file
            StorageFile stickerMockup = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/Images/stickerMockup.png"));
            BitmapDecoder imageDecoder;
            using (var imageStream = await stickerMockup.OpenAsync(FileAccessMode.Read))
            {
                imageDecoder = await BitmapDecoder.CreateAsync(imageStream);
            }
            CanvasDevice device = CanvasDevice.GetSharedDevice();
            CanvasRenderTarget renderTarget = new CanvasRenderTarget(device, imageDecoder.PixelWidth, imageDecoder.PixelHeight, 96);

            //Draw session stickerWithInformation
            using (var ds = renderTarget.CreateDrawingSession())
            {
                ds.Clear(Colors.White);
                CanvasBitmap image = await CanvasBitmap.LoadAsync(device, stickerMockup.Path, 96);
                ds.DrawImage(image);
                ds.DrawText(visitor.FirstName + " " + visitor.LastName, new System.Numerics.Vector2(10, 85), Colors.Black, new CanvasTextFormat() { FontSize = 20, FontWeight = FontWeights.Bold, FontFamily = "SegoeUI" });
                ds.DrawText("Company: " + visitor.CompanyName, new System.Numerics.Vector2(10, 115), Colors.Black, new CanvasTextFormat() { FontSize = 20, FontFamily = "SegoeUI" });
                ds.DrawText("Bage number: " + visitor.BadgeNumber, new System.Numerics.Vector2(10, 145), Colors.Black, new CanvasTextFormat() { FontSize = 20, FontFamily = "SegoeUI" });
            }

            //Output stickerWithInformation
            var stickerWithInformation = await Windows.Storage.ApplicationData.Current.LocalFolder.CreateFileAsync("generatedSticker.png", CreationCollisionOption.ReplaceExisting);

            using (var fileStream = await stickerWithInformation.OpenAsync(FileAccessMode.ReadWrite))
            {
                await renderTarget.SaveAsync(fileStream, CanvasBitmapFileFormat.Png, 1f);
            }

            //Merge two images
            var image1 = await BitmapFactory.FromContent(new Uri("ms-appdata:///local/generatedSticker.png"), BitmapPixelFormat.Unknown);

            image1.Blit(new Rect(250, 65, 140, 140), qrWritableBitmap, new Rect(0, 0, qrWritableBitmap.PixelWidth, qrWritableBitmap.PixelHeight));
            Guid BitmapEncoderGuid = BitmapEncoder.PngEncoderId;

            using (IRandomAccessStream stream = await stickerWithInformation.OpenAsync(FileAccessMode.ReadWrite))
            {
                BitmapEncoder encoder = await BitmapEncoder.CreateAsync(BitmapEncoderGuid, stream);
                var pixelStream = image1.PixelBuffer.AsStream();
                byte[] pixels = new byte[pixelStream.Length];
                await pixelStream.ReadAsync(pixels, 0, pixels.Length);

                encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Ignore,
                                    (uint)image1.PixelWidth,
                                    (uint)image1.PixelHeight,
                                    96.0,
                                    96.0,
                                    pixels);
                await encoder.FlushAsync();
            }


            await this.PrintMergedSticker();
        }

        private WriteableBitmap GenerateQRCode(Visitor visitor)
        {
            var options = new QrCodeEncodingOptions()
            {
                DisableECI = true,
                CharacterSet = "UTF-8",
                Width = 50,
                Height = 50
            };

            BarcodeWriter writer = new BarcodeWriter();
            writer.Format = BarcodeFormat.QR_CODE;
            writer.Options = options;
            return writer.Write(visitor.BadgeNumber);
        }

        #endregion

        #region PrintSticker

        private async Task PrintMergedSticker()
        {
            StorageFile stickerMockup = await Windows.Storage.ApplicationData.Current.LocalFolder.CreateFileAsync("generatedSticker.png", CreationCollisionOption.OpenIfExists);

            using (FileRandomAccessStream stream = (FileRandomAccessStream)await stickerMockup.OpenAsync(FileAccessMode.Read))
            {
                if(this.ImageToProintUri == null)
                {
                    this.ImageToProintUri = new BitmapImage();
                }

                this.ImageToProintUri.SetSource(stream);
            }

            printHelper = new PrintHelper(stickerToPringGrid);
            await printHelper.ShowPrintUIAsync("Print the sticker", true);

            printHelper.OnPrintCanceled += OnPrintCanceledHandler;
            printHelper.OnPrintSucceeded += OnPrintSucceededHandler;
            printHelper.OnPrintFailed += OnPrintFailedHandler;
            stickerToPringGrid = null;
        }

        private void OnPrintCanceledHandler()
        {
            this.ImageToProintUri = null;
            printHelper.ClearListOfPrintableFrameworkElements();
            printHelper.RemoveFrameworkElementToPrint(this.stickerToPringGrid);
            printHelper.Dispose();
        }

        private void OnPrintSucceededHandler()
        {
            this.ImageToProintUri = null;
            printHelper.RemoveFrameworkElementToPrint(this.stickerToPringGrid);
            printHelper.ClearListOfPrintableFrameworkElements();
            printHelper.Dispose();
        }

        private void OnPrintFailedHandler()
        {
            this.ImageToProintUri = null;
            printHelper.RemoveFrameworkElementToPrint(this.stickerToPringGrid);
            printHelper.ClearListOfPrintableFrameworkElements();
            printHelper.Dispose();
        }

        #endregion
    }

    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public BitmapImage Photo { get; set; }
    }

    public class NewVisitor
    {
        public int Id { get; set; }
        public string BadgeNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string CompanyName { get; set; }
        public DateTime CheckInTime { get; set; }
    }

    public class Visitor
    {
        public int Id { get; set; }
        public string BadgeNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string CheckInTime { get; set; }
    }
}
