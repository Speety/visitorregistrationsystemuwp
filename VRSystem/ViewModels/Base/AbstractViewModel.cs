﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;

namespace VRSystem.ViewModels.Base
{
    public class AbstractViewModel : DependencyObject, IDisposable, INotifyPropertyChanged
    {
        private DataTemplate modelTemplate;

        public AbstractViewModel()
        {
        }

        public virtual void Dispose()
        {
            this.ModelTemplate = null;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            var eventHandler = this.PropertyChanged;
            eventHandler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual void RaisePropertyChanged<T>(Expression<Func<T>> propertyExpression)
        {
            var handler = this.PropertyChanged;

            if (handler != null)
            {
                var propertyName = GetPropertyName(propertyExpression);
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected static string GetPropertyName<T>(Expression<Func<T>> propertyExpression)
        {
            if (propertyExpression == null)
            {
                throw new ArgumentNullException(nameof(propertyExpression));
            }

            var body = propertyExpression.Body as MemberExpression;

            if (body == null)
            {
                throw new ArgumentException("Invalid argument", nameof(propertyExpression));
            }

            var property = body.Member as PropertyInfo;

            if (property == null)
            {
                throw new ArgumentException("Argument is not a property", nameof(propertyExpression));
            }

            return property.Name;
        }


        public DataTemplate ModelTemplate
        {
            get { return this.modelTemplate; }
            set
            {
                if (this.modelTemplate != value)
                {
                    this.modelTemplate = value;
                    this.RaisePropertyChanged();
                }
            }
        }

    }
}
