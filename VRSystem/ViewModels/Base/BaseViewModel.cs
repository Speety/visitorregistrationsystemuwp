﻿using System;
using System.Threading.Tasks;
using VRSystem.DataAccess;
using Windows.ApplicationModel;
using Windows.Foundation.Metadata;
using Windows.UI;
using Windows.UI.ViewManagement;

namespace VRSystem.ViewModels.Base
{
    public class BaseViewModel : AbstractViewModel
    {
        #region Fields
        protected const string InsufficientPermissionsErrorCode = "403";
        public bool IsConnected => InternetConnection.IsConnected;

        protected string templateDictionary;

        #endregion

        protected BaseViewModel()
        {
        }


        public override void Dispose()
        {
            base.Dispose();
        }

        protected async Task Refresh(object parameter = null)
        {
            if (ApiInformation.IsTypePresent("Windows.UI.ViewManagement.ApplicationView"))
            {
                var titleBar = ApplicationView.GetForCurrentView().TitleBar;
                if (titleBar != null)
                {
                    titleBar.ButtonBackgroundColor = Color.FromArgb(0, 31, 31, 31);
                    titleBar.ButtonForegroundColor = Colors.White;
                    titleBar.BackgroundColor = Color.FromArgb(0, 31, 31, 31);
                    titleBar.ForegroundColor = Colors.White;
                }
                
            }
            var task = OnRefresh(parameter);
            await task;
            if (task.Result != null)
            {
            }
        }

        protected virtual async Task<Exception> OnRefresh(object parameter = null)
        {
            Exception exception = null;
            var task = new Task(() =>
            {
                try
                {
                }
                catch (Exception ex)
                {
                    exception = ex;
                }
            });
            task.Start();
            await task;
            return exception;
        }

    }
}
