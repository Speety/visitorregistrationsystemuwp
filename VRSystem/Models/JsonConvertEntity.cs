﻿using VRSystem.ViewModels;

namespace VRSystem.Models
{
    public class JsonConvertEntity
    {
        public Employee Employee { get; set; }

        public NewVisitor Visitor { get; set; }
    }
}
