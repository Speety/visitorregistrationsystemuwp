﻿namespace VRSystem.Models
{
    public class ChangesMadeOnServer
    {
        public bool IsChangesMadeToEmployees { get; set; }

        public bool IsChangesMadeToVisitors { get; set; }
    }
}
