﻿namespace VRSystem.Models.Enums
{
    public enum InputInformationType
    {
        NameOrLastName,
        CompanyName,
        PhoneNumber
    }
}
